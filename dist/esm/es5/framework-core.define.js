// FrameworkCore: Custom Elements Define Library, ES Module/ES5 Target
import { defineCustomElement } from './framework-core.core.js';
import {
  Button,
  Card,
  CardContent,
  CardHeader,
  Checkbox,
  Icon,
  Input,
  Label,
  List,
  Rgpd,
  RippleEffect,
  Toolbar,
  ToolbarTitle,
  UploaderComponent
} from './framework-core.components.js';

export function defineCustomElements(window, opts) {
  defineCustomElement(window, [
    Button,
    Card,
    CardContent,
    CardHeader,
    Checkbox,
    Icon,
    Input,
    Label,
    List,
    Rgpd,
    RippleEffect,
    Toolbar,
    ToolbarTitle,
    UploaderComponent
  ], opts);
}