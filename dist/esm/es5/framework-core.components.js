// FrameworkCore: Host Data, ES Module/ES5 Target

export var Button = ["ion-button",function(o){return("ios"==o.mode?import("./mjujsap6.js"):import("./ij9l53ke.js")).then(function(m){return m.IonButton})},1,[["buttonType",2,0,"button-type",2],["color",1,0,1,2],["disabled",1,0,1,3],["el",7],["expand",1,0,1,2],["fill",1,0,1,2],["href",1,0,1,2],["keyFocus",5],["mode",1,0,1,2],["routerDirection",1,0,"router-direction",2],["shape",1,0,1,2],["size",1,0,1,2],["strong",1,0,1,3],["type",1,0,1,2],["win",3,0,0,0,"window"]]];

export var Card = ["ion-card",function(o){return("ios"==o.mode?import("./qr4xqfa9.js"):import("./bm99xhq8.js")).then(function(m){return m.IonCard})},1,[["color",1,0,1,2],["mode",1,0,1,2]]];

export var CardContent = ["ion-card-content",function(o){return("ios"==o.mode?import("./qr4xqfa9.js"):import("./bm99xhq8.js")).then(function(m){return m.IonCardContent})},1,[["color",1,0,1,2],["mode",1,0,1,2]]];

export var CardHeader = ["ion-card-header",function(o){return("ios"==o.mode?import("./qr4xqfa9.js"):import("./bm99xhq8.js")).then(function(m){return m.IonCardHeader})},1,[["color",1,0,1,2],["mode",1,0,1,2],["translucent",1,0,1,3]]];

export var Checkbox = ["ion-checkbox",function(o){return("ios"==o.mode?import("./qr4xqfa9.js"):import("./bm99xhq8.js")).then(function(m){return m.IonCheckbox})},1,[["checked",2,0,1,3],["color",1,0,1,2],["disabled",1,0,1,3],["el",7],["keyFocus",5],["mode",1,0,1,2],["name",1,0,1,2],["value",1,0,1,2]]];

export var Icon = ["ion-icon",function(o){return("ios"==o.mode?import("./sshqa9n4.js"):import("./yevyqacj.js")).then(function(m){return m.IonIcon})},1,[["ariaLabel",1,0,"aria-label",2],["color",1,0,1,2],["doc",3,0,0,0,"document"],["el",7],["icon",1,0,1,2],["ios",1,0,1,2],["isServer",3,0,0,0,"isServer"],["isVisible",5],["md",1,0,1,2],["mode",3,0,0,0,"mode"],["name",1,0,1,2],["resourcesUrl",3,0,0,0,"resourcesUrl"],["size",1,0,1,2],["src",1,0,1,2],["svgContent",5],["win",3,0,0,0,"window"]]];

export var Input = ["ion-input",function(o){return("ios"==o.mode?import("./sshqa9n4.js"):import("./yevyqacj.js")).then(function(m){return m.IonInput})},1,[["accept",1,0,1,2],["autocapitalize",1,0,1,2],["autocomplete",1,0,1,2],["autocorrect",1,0,1,2],["autofocus",1,0,1,3],["clearInput",1,0,"clear-input",3],["clearOnEdit",2,0,"clear-on-edit",3],["debounce",1,0,1,4],["disabled",1,0,1,3],["el",7],["inputmode",1,0,1,2],["max",1,0,1,2],["maxlength",1,0,1,4],["min",1,0,1,2],["minlength",1,0,1,4],["multiple",1,0,1,3],["name",1,0,1,2],["pattern",1,0,1,2],["placeholder",1,0,1,2],["readonly",1,0,1,3],["required",1,0,1,3],["results",1,0,1,4],["size",1,0,1,4],["spellcheck",1,0,1,3],["step",1,0,1,2],["type",1,0,1,2],["value",2,0,1,2]]];

export var Label = ["ion-label",function(o){return("ios"==o.mode?import("./sshqa9n4.js"):import("./yevyqacj.js")).then(function(m){return m.IonLabel})},1,[["color",1,0,1,2],["el",7],["getText",6],["mode",1,0,1,2],["position",1,0,1,2]]];

export var List = ["ion-list",function(o){return("ios"==o.mode?import("./qr4xqfa9.js"):import("./bm99xhq8.js")).then(function(m){return m.IonList})},1,[["closeSlidingItems",6],["getOpenItem",6],["lines",1,0,1,2],["setOpenItem",6]]];

export var RippleEffect = ["ion-ripple-effect",function(o){return("ios"==o.mode?import("./mjujsap6.js"):import("./ij9l53ke.js")).then(function(m){return m.IonRippleEffect})},1,[["addRipple",6],["doc",3,0,0,0,"document"],["el",7],["enableListener",3,0,0,0,"enableListener"],["queue",3,0,0,0,"queue"],["tapClick",1,0,"tap-click",3]],0,[["mousedown","mouseDown",1,1],["parent:ionActivated","ionActivated",1],["touchstart","touchStart",1,1]]];

export var ToolbarTitle = ["ion-title",function(o){return("ios"==o.mode?import("./qr4xqfa9.js"):import("./bm99xhq8.js")).then(function(m){return m.IonTitle})},1,[["color",1,0,1,2],["mode",1,0,1,2]]];

export var Toolbar = ["ion-toolbar",function(o){return("ios"==o.mode?import("./mjujsap6.js"):import("./ij9l53ke.js")).then(function(m){return m.IonToolbar})},1,[["color",1,0,1,2],["config",3,0,0,0,"config"],["mode",1,0,1,2],["translucent",1,0,1,3]]];

export var Rgpd = ["rgpd-component",function(){return(import("./hkgofrzn.js")).then(function(m){return m.RgpdComponent})},1,[["opcionesCheck",1],["rgpd",7],["showModal",5],["showText",5],["texto",1,0,1,2],["titulo",1,0,1,2]],1];

export var UploaderComponent = ["uploader-component",function(){return(import("./hkgofrzn.js")).then(function(m){return m.UploaderComponent})},1,[["document",5],["documents",5],["placeholder",1,0,1,2],["showInput",1,0,"show-input",3],["textBoton",1,0,"text-boton",2],["textInfo",1,0,"text-info",2],["titulo",1,0,1,2],["uploader",7]],1];