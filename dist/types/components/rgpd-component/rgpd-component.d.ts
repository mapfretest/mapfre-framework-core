import '../../stencil.core';
import { EventEmitter } from '../../stencil.core';
import '@ionic/core';
export declare class Rgpd {
    titulo: string;
    texto: string;
    opcionesCheck: any[];
    showModal: boolean;
    showText: boolean;
    rgpd: HTMLElement;
    mapfreRgpdSignedEvent: EventEmitter;
    componentDidLoad(): void;
    stringControl(texto: any): any;
    showTextComplete(texto: any): JSX.Element;
    enviarForm(): void;
    isButtonDisabled(): void;
    handleChange(event: any, opcion: any): void;
    render(): JSX.Element;
}
