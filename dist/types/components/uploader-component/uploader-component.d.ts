import '../../stencil.core';
import { EventEmitter } from '../../stencil.core';
import '@ionic/core';
export declare class UploaderComponent {
    titulo: string;
    placeholder: string;
    textBoton: string;
    textInfo: string;
    showInput: boolean;
    uploader: HTMLElement;
    mapfreUploaderOnInputClickedEvent: EventEmitter;
    mapfreUploaderOnButtonClickedEvent: EventEmitter;
    document: any;
    documents: any;
    constructor();
    componentDidLoad(): void;
    render(): JSX.Element;
    onInputClicked(event: any): void;
    onButtonClicked(event: any): void;
}
