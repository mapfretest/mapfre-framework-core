import './stencil.core';
/**
 * This is an autogenerated file created by the Stencil build process.
 * It contains typing information for all components that exist in this project
 * and imports for stencil collections that might be configured in your stencil.config.js file
 */

import './stencil.core';

declare global {
  namespace JSX {
    interface Element {}
    export interface IntrinsicElements {}
  }
  namespace JSXElements {}

  interface HTMLStencilElement extends HTMLElement {
    componentOnReady(): Promise<this>;
    componentOnReady(done: (ele?: this) => void): void;

    forceUpdate(): void;
  }

  interface HTMLAttributes {}
}

import '@ionic/core';
import 'ionicons';


declare global {

  namespace StencilComponents {
    interface RgpdComponent {
      'opcionesCheck': any[];
      'texto': string;
      'titulo': string;
    }
  }

  interface HTMLRgpdComponentElement extends StencilComponents.RgpdComponent, HTMLStencilElement {}

  var HTMLRgpdComponentElement: {
    prototype: HTMLRgpdComponentElement;
    new (): HTMLRgpdComponentElement;
  };
  interface HTMLElementTagNameMap {
    'rgpd-component': HTMLRgpdComponentElement;
  }
  interface ElementTagNameMap {
    'rgpd-component': HTMLRgpdComponentElement;
  }
  namespace JSX {
    interface IntrinsicElements {
      'rgpd-component': JSXElements.RgpdComponentAttributes;
    }
  }
  namespace JSXElements {
    export interface RgpdComponentAttributes extends HTMLAttributes {
      'onMapfreRgpdSignedEvent'?: (event: CustomEvent) => void;
      'opcionesCheck'?: any[];
      'texto'?: string;
      'titulo'?: string;
    }
  }
}


declare global {

  namespace StencilComponents {
    interface UploaderComponent {
      'placeholder': string;
      'showInput': boolean;
      'textBoton': string;
      'textInfo': string;
      'titulo': string;
    }
  }

  interface HTMLUploaderComponentElement extends StencilComponents.UploaderComponent, HTMLStencilElement {}

  var HTMLUploaderComponentElement: {
    prototype: HTMLUploaderComponentElement;
    new (): HTMLUploaderComponentElement;
  };
  interface HTMLElementTagNameMap {
    'uploader-component': HTMLUploaderComponentElement;
  }
  interface ElementTagNameMap {
    'uploader-component': HTMLUploaderComponentElement;
  }
  namespace JSX {
    interface IntrinsicElements {
      'uploader-component': JSXElements.UploaderComponentAttributes;
    }
  }
  namespace JSXElements {
    export interface UploaderComponentAttributes extends HTMLAttributes {
      'onMapfreUploaderOnButtonClickedEvent'?: (event: CustomEvent) => void;
      'onMapfreUploaderOnInputClickedEvent'?: (event: CustomEvent) => void;
      'placeholder'?: string;
      'showInput'?: boolean;
      'textBoton'?: string;
      'textInfo'?: string;
      'titulo'?: string;
    }
  }
}

declare global { namespace JSX { interface StencilJSX {} } }

export declare function defineCustomElements(window: any): void;