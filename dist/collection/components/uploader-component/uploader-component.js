export class UploaderComponent {
    constructor() { }
    componentDidLoad() {
        let boton = this.uploader.shadowRoot.querySelector("ion-button");
        let input = this.uploader.shadowRoot.querySelector("ion-input");
        if (this.document) {
            input.value = this.document.name;
        }
        if (this.showInput == true && !this.document.name) {
            boton.setAttribute("disabled", "true");
        }
    }
    render() {
        return (h("div", { class: "up-container" },
            h("ion-label", { color: "danger", class: "up-title" }, this.titulo),
            this.showInput == true &&
                h("div", { class: "up-input-block" },
                    h("ion-input", { type: "text", disabled: true, placeholder: this.placeholder, class: "up-input", onClick: (ev) => this.onInputClicked(ev) }),
                    h("ion-icon", { id: 'upload-icon', name: 'cloud-upload' })),
            h("ion-button", { color: "danger", class: "up-button", onClick: (ev) => this.onButtonClicked(ev) }, this.textBoton),
            h("ion-toolbar", { class: "up-toolbar" },
                h("div", { class: "up-info-icon" },
                    h("ion-icon", { id: "info-icon", color: "danger", name: "information-circle-outline" })),
                h("div", { class: "up-info-text" },
                    h("p", null, this.textInfo)))));
    }
    onInputClicked(event) {
        /** Abrir explorador de archivos */
        let objEvento;
        objEvento = {
            event: event,
            showInput: this.showInput,
            openExplorer: true,
            sendDocument: false
        };
        this.mapfreUploaderOnInputClickedEvent.emit(objEvento);
    }
    onButtonClicked(event) {
        let boton = this.uploader.shadowRoot.querySelector("ion-button");
        let objEvento;
        if (this.showInput == true && !boton.getAttribute("disabled")) {
            /* Subir archivo */
            objEvento = {
                event: event,
                showInput: this.showInput,
                openExplorer: false,
                sendDocument: true,
                documentsUpload: this.documents
            };
        }
        else if (this.showInput == false) {
            /* Elegir archivo para subir y subirlo */
            objEvento = {
                event: event,
                showInput: this.showInput,
                openExplorer: true,
                sendDocument: true
            };
        }
        this.mapfreUploaderOnButtonClickedEvent.emit(objEvento);
    }
    static get is() { return "uploader-component"; }
    static get encapsulation() { return "shadow"; }
    static get properties() { return {
        "document": {
            "state": true
        },
        "documents": {
            "state": true
        },
        "placeholder": {
            "type": String,
            "attr": "placeholder"
        },
        "showInput": {
            "type": Boolean,
            "attr": "show-input"
        },
        "textBoton": {
            "type": String,
            "attr": "text-boton"
        },
        "textInfo": {
            "type": String,
            "attr": "text-info"
        },
        "titulo": {
            "type": String,
            "attr": "titulo"
        },
        "uploader": {
            "elementRef": true
        }
    }; }
    static get events() { return [{
            "name": "mapfreUploaderOnInputClickedEvent",
            "method": "mapfreUploaderOnInputClickedEvent",
            "bubbles": true,
            "cancelable": true,
            "composed": true
        }, {
            "name": "mapfreUploaderOnButtonClickedEvent",
            "method": "mapfreUploaderOnButtonClickedEvent",
            "bubbles": true,
            "cancelable": true,
            "composed": true
        }]; }
    static get style() { return "/**style-placeholder:uploader-component:**/"; }
}
