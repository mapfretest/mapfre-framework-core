export class Rgpd {
    constructor() {
        this.showModal = true;
        this.showText = false;
    }
    componentDidLoad() {
        this.isButtonDisabled();
    }
    stringControl(texto) {
        if (texto.length > 50) {
            return (texto.substr(0, 50));
        }
        else {
            return texto;
        }
    }
    showTextComplete(texto) {
        return (h("div", { class: "textComplete" },
            h("p", null, texto)));
    }
    enviarForm() {
        let boton = this.rgpd.shadowRoot.querySelector('.btn-acept');
        if (!boton.getAttribute('disabled')) {
            this.mapfreRgpdSignedEvent.emit(this.opcionesCheck);
            let modalRGPD = this.rgpd.shadowRoot.querySelector('.modalRgpd');
            modalRGPD.style.display = 'none';
        }
    }
    isButtonDisabled() {
        let error = 0;
        let boton = this.rgpd.shadowRoot.querySelector('.btn-acept');
        for (let op in this.opcionesCheck) {
            if (this.opcionesCheck[op].required == true && this.opcionesCheck[op].status == false) {
                error++;
            }
        }
        if (error > 0) {
            if (!boton.getAttribute('disabled')) {
                boton.setAttribute('disabled', 'disabled');
            }
        }
        else {
            boton.removeAttribute('disabled');
        }
    }
    handleChange(event, opcion) {
        let target = event.target;
        let checked = target.checked;
        for (var op in this.opcionesCheck) {
            if (this.opcionesCheck[op].Id == opcion) {
                this.opcionesCheck[op].status = checked;
            }
        }
        this.isButtonDisabled();
    }
    render() {
        if (this.showModal) {
            return (h("ion-card", { id: "rgpd", class: "modalRgpd" },
                h("ion-card-header", null,
                    h("ion-toolbar", { class: "center" },
                        h("ion-title", null, this.titulo))),
                h("ion-card-content", { class: "left" },
                    h("p", { class: "pModal" }, this.texto),
                    h("ion-list", null, this.opcionesCheck.map((opcion) => h("div", { class: "customItem" },
                        h("ion-checkbox", { name: opcion.Id, checked: opcion.status, onInput: event => this.handleChange(event, opcion.Id) }),
                        h("label", null,
                            this.stringControl(opcion.text),
                            opcion.text.length > 50 && h("a", { class: "aviso", onClick: () => this.showText = !this.showText }, "Aviso Privacidad")),
                        this.showText && opcion.text.length > 50 && this.showTextComplete(opcion.text))))),
                h("div", { class: "center bottom" },
                    h("ion-button", { type: "submit", class: "btn-acept", onClick: () => this.enviarForm() }, "ACEPTO"))));
        }
    }
    static get is() { return "rgpd-component"; }
    static get encapsulation() { return "shadow"; }
    static get properties() { return {
        "opcionesCheck": {
            "type": "Any",
            "attr": "opciones-check"
        },
        "rgpd": {
            "elementRef": true
        },
        "showModal": {
            "state": true
        },
        "showText": {
            "state": true
        },
        "texto": {
            "type": String,
            "attr": "texto"
        },
        "titulo": {
            "type": String,
            "attr": "titulo"
        }
    }; }
    static get events() { return [{
            "name": "mapfreRgpdSignedEvent",
            "method": "mapfreRgpdSignedEvent",
            "bubbles": true,
            "cancelable": true,
            "composed": true
        }]; }
    static get style() { return "/**style-placeholder:rgpd-component:**/"; }
}
