## Getting Started

To start building a new web component using Stencil, clone this repo to a new directory:

```bash
git clone https://bitbucket.org/josemlinaresdxd/framework-core.git framework-core
cd framework-core
git remote rm origin
```

and run:

```bash
npm install
npm start
```

To watch for file changes during develop, run:

```bash
npm run dev
```

To build the component for production, run:

```bash
npm run build
```

To run the unit tests for the components, run:

```bash
npm test
```
